#!/bin/bash


#
#This program is free software: you can redistribute it 
#and/or modify it under the terms of the GNU General Public License 
#as published by the Free Software Foundation, 
#either version 3 of the License, or (at your option) any later version.
#
#This program is distributed in the hope that it will be useful, 
#but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
#or FITNESS FOR A PARTICULAR PURPOSE. 
#See the GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License 
#along with this program. If not, see <https://www.gnu.org/licenses/>. 
#

#
# Script qui extrait des logs Moodle la liste des taches programmées exécutées
# puis les  enregistre dans une base de données SQLite ($BDD)
# et y ajoute de quoi les regrouper par intervalle de temps 
# (permettant ainsi de connaitre la ilste des taches qui s'exécutent dans chaque intervalle)
#

FOLDER=/var/log/moodle
SOURCE=cron_*.log
CSV=tmp.csv
BDD=moodle-logs.db

cd $FOLDER

echo "Préparation $CSV temporaire..."

[ -e $CSV ] && rm -v $CSV
for f in $SOURCE
do 
echo "Analyse de $f"
# date du fichier de log :
d=$(ls -l $f | cut -d ' ' -f 6,7,8)

# 1. extraction des lignes importantes (egrep ...)
# 2. substitution de caracteres remplacement des sauts de lignes \x10 par des \x13
# 3. substitution des quotes \x22 par d'autres \xab (\253 en octal)
# 4. insertion de saut de lignes avant chaque 'Execute' (sed ...) pour avoir une ligne par exécution et suppression de la 1ere ligne vide (sed ...)
# 5. suppression de la 1er ligne
# 6. insertion du nom de fichier de log et de sa date en debut de ligne (2 ier champ dans le csv)
# 7. awk pour mettre entre quote le 3ieme champs (output) et créer autant de champs vides que necessaire ensuite
# 8. restauration des quotes, mais avec échappement '\"'
# 9. restauration des sauts de lignes
cat $f | \
tr '\n' '\r' | \
tr '"' '\253' | \
sed 's/Execute /\n/g' | \
sed '1d' | \
sed "s/^/$f;$d;/" | \
awk -F';' '{print $1";"$2";\""$3"\";;;;;;;;"}' | \
sed 's/\xab/""/g' | \
sed 's/\r/\n/g' >> $CSV
echo >> $CSV
done


#Suppression des éventuelles lignes vides
sed -i "/^\$/d" $CSV

# debug :
# echo $( wc -l $CSV | egrep -o [0-9]*) lignes extraites

echo
echo "Préparation $BDD, table tasks..."
# import du csv dans une BD SQLite
# et peuplement des colonnes non encore renseignées
# par extraction des données depuis le texte des 5 lignes originelles (colonnes ligne1 à ligne5)
[ -e $BDD ] && rm -v $BDD

sqlite3 $BDD << eof
.mode csv
.separator ;
create table tasks(
	file text,
	date text,
	output text,
	type text,
	classname text,
	started text,
	stopped text,
	duration text,
	failed text,
	has_exception text,
	has_error text);
.import $CSV tasks

-- type
update tasks 
set type = 'scheduled'
where output like 'scheduled%';
update tasks 
set type = 'adhoc'
where output like 'adhoc%';

-- classname
update tasks 
set classname = 
	substr(
		output,
		instr(output, '(') +1, -- pos. (
		instr(output, char(10)) - instr(output, '(') -2 -- longueur jusquau new line
		)
where type = 'scheduled';
update tasks 
set classname = replace(
		substr(output, 1, -1+instr(output, char(10))), -- 1ere ligne
		'adhoc task: ', 
		'')
where type = 'adhoc';

-- started
update tasks 
set started = substr(
	substr(output, 
		1, 
		instr(output, '. Current')-1),
	-8);

-- duration
update tasks 
set duration =substr(
	output, 
	instr(output, 'dbqueries'||char(10)) + 19, 
	instr(output, 'seconds'||char(10)) - instr(output, 'dbqueries'||char(10)) - 19
	);
update tasks
set duration = null
where not output like '%second%';

-- stopped
update tasks 
set stopped = time(started, '+' || duration || ' seconds');
update tasks
set stopped = null
where not output like '%second%';


-- failed
update tasks 
set failed = 0
where output like '%task complete:%';
update tasks 
set failed = 1
where output like '%task failed:%';

-- has_exception
update tasks 
set has_exception = 0;
update tasks
set has_exception = 1
where output like '%Exception%';

-- has_error
update tasks 
set has_error = 0;
update tasks
set has_error = 1
where lower(output) like '%error%';

.exit
eof
[ -e $CSV ] && rm -v $CSV

echo
echo "Generation des slots dans $BDD..."
# generation d'une table slots possédant 1 ligne par minute de la journée
# generation d'un csv temporaire :
for h in {0..23}
do
echo "de $h:00 à $h:59"
for m in {0..59}
do
t="$(printf '%02d' $h):$(printf '%02d' $m):00"
echo "$t;0" >> $CSV
done
done
# import de ce csv :
sqlite3 $BDD << eof
.mode csv
.separator ;
create table slots (start text, stop text);
.import $CSV slots
update slots set stop = time(start, '+1 minutes');
.exit
eof
[ -e $CSV ] && rm -v $CSV

echo
echo "Génération des tables et vues supplémentaires..."
# Génération des tables et vues pour analyse répartition des exécutions dans le temps
sqlite3 $BDD << eof
create table if not exists tasks_per_slot as 
select * 
from slots s 
	join tasks t 
		on t.started < s.stop and t.stopped >= s.start;

eof


echo
echo "Statistiques d'échec et affichage final..."
sqlite3 $BDD << eof
-- Table des types d erreur connues :
create table known_failures (
	composant text,
	name text, 
	filtre text)
;

-- Definition des types d erreurs connues :
insert into known_failures (composant, name, filtre) values 
	('redis', 'read error', '%read error on connection to ecampus-redis.unicaen.fr%'),
	('redis', 'went away', '%Redis server % went away%'),
	('système de fichiers', 'droits pool fichiers', '%Impossible de créer les dossiers du pool de fichiers locaux. Veuillez vérifier les droits%'),
	('base de données', 'erreur lecture', '%Erreur de lecture de la base de données%'),
	(null, 'course could not be deleted', '%Erreur de programmation détectée. Ceci doit être corrigé par un programmeur : The course module%could not be deleted%'),
	(null, 'Id module invalide', '%Identifiant de module de cours non valide%'),
	(null, 'courses do not exist', '%following courses do not exist%'),
	(null, 'could not load/find the RSS feed', '%could not load/find the RSS feed%'),
	(null, 'get_record() more than one record', '%get_record()%found more than one record%'),
	('compilatio', 'invalid compilatio response', '%invalid compilatio response received%'),
	(null, 'Error reading from the external enrolment', '%Error reading data from the external enrolment table%'),
	(null, 'encounters an error', '%encounters an error%')
;

-- Statistiques :
.header on
.mode column
select t.date, 
  t.classname, 
  e.composant, 
  e.name, 
  sum(failed) as nbre_failed,
  sum(has_exception) as nbre_exception,
  sum(has_error) as nbre_error,
  a.sur
from tasks t -- les taches
  left join known_failures e 
    on t.output like e.filtre
      or t.output like e.filtre -- les erreurs typiques
  join (
    select date, classname, count(*) as sur
    from tasks
    group by date, classname
  ) as a -- all (nbre d'executions réussies ou pas, par date et tache)
    on a.classname = t.classname 
    and a.date = t.date
group by t.date, t.classname, e.composant, e.name
having nbre_failed + nbre_exception + nbre_error > 0
;


eof


echo "$BDD est maintenant visitable."
