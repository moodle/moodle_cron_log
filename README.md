# moodle_cron_log

This project aims to give tools to extract data from Moodle cron's log files in such a way they can be easily analysed inside a database, using SQL language.

## Script import_into_DB.sh
### Prerequistes

The supplied script, `import_into_DB.sh`, has been designed considering :
* sqlite3 is intalled on the server and executed calling `sqlite3` command (without speciying its path)
* Moodle cron log files are stored in a folder `/var/log/mmodle`, for example by having a line like this in the server crontab :

  ```
  php -f /path/to/moodle/root/folder/admin/cli/cron.php > /var/log/moodle/cron_$(date +\%H\%M).log 2>&1
  ```
* these files are named `cron_hhmm.log` (for example 'cron_1013.log' is the log file associated with the cron launched at 10h13)
* the shell command `ls -l $f | cut -d ' ' -f 6,7,8` is efficient to extract the file creation date

### Principle of operation

This script 
1. extracts recursively, from all cron log files in the directory, all interesting datas as a unique temporary csv file. This one contains a set of lines (one per task), and for each a set of columns (separated by a ';' character) for each data.
2. creates a SQLite database, `moodle-logs.db`, and import inside it this csv as a table, `tasks`
3. creates inside this database another table, `slots`, with all minutes that compose a day
4. creates a table, `tasks_per_slot`, from the joins of both preceeding tables, supplying therefore, minute after minute, the list of task running or still in progress during that minute
5. creates a table, `known_failures`, containing predefined failures description in order to give more informations inside final statistics
6. excutes a final query to get statistics about detected task failures

### Limitations

The script should be seen as an **alpha version**. It is too young to be considered "mature".

The table, `known_failures`, has been created considering failures seen recently in our university. Its content is not exhaustive, and probably needs to be extended.

### Database description

The resulting database own 4 tables 
  * Table `tasks` : 1 ligne par exécution de tache, avec les colonnes :
    * `file` = name of the cron log file from which the task execution has been extracted
    * `date` = file creation date
    * `ligne1` = the 1st interesting line in the file about the task execution (several usefull datas)
    * `ligne2` = the second (should contains the starting time of the execution)
    * `ligne3` = the third line or another in between first and last (the first that may contains some exception notice)
    * `ligne4` = the next-to-last interesting line (including duration)
    * `ligne5` = the last (contains result and sometime failure message)
    * `type` = '//scheduled//' or '//adhoc//' (first are planified, seconds are executed when possible)
    * `classname` = task name (as seen in Moodle code or in administration pages)
    * `started` = starting time
    * `stopped` = stoping time
    * `duration` = duration (in seconds)
    * `failed` = 0 (task ended without problem) ou 1 (failing task)
    * `exception` = 0 (no problem) ou 1 (at least 1 exception raised)
  * Table `slots` : 1 line per minute in the day
  * Table `tasks_per_slot` : (joins between both previous tables) 1 line per executed task or task still in progress, within the relevant minute (if a task execution last for instance 6 minutes, then 6 lines will be generated for it in this table)
  * Table `known_failures` : 1 ligne per failure known type
    * `composant` = the impacted element
    * `name` = a name for this kind of error
    * `filtre` = a filtering string that could be used as is, inside a SQL query (with a `LIKE` operator), to get relevant lines

### Usage

#### SQL clients

To explore results, a SQLite client should be used. Many open source software exist. Such as :

* Command line client, `sqlite3`, itself.  Examples of use :

  ```bash
  sqlite3 moodle-db.logs ".schema task" # to see task's columns
  sqlite3 moodle-db.logs "select count(*) from tasks;"
  ```
* https://sqlitebrowser.org/ a lightweight graphic client
* https://dbeaver.io/ a universal full featured client, able to connect to SQLite dabatase, but also to other types of databases such as mariadb, Oracle, and many others.


#### Queries examples

Below, some examples of what could be done with this database.

The first example, statistics of failures, can be found at the end of the script.

Duration statistics for a specific task that may contains a specific word in the error message ('redis' for instance) :
```sql
select 
	min(cast(duration as double)) as duree_min,
	max(cast(duration as double)) as duree_max,
	sum(case when (ligne3 || ligne5) like '%redis%' then 1 else 0 end) as pb_redis,
	count(*) as "on",
	100*sum(case when (ligne3 || ligne5) like '%redis%' then 1 else 0 end)/count(*) as "in_%"
from tasks t 
group by floor(log(floor(cast(duration as double))+1)) -- logarithmic categorization
;
```

Display, minute after minute, the amount of simultaneous crons in progress :
```sql
select 
	"date",
	"start", 
	count(distinct file) as crons_cnt,
	count(distinct (CASE WHEN "type" = 'scheduled' THEN file ELSE null END)) as scheduled_crons_cnt,
	count(distinct (CASE WHEN "type" = 'adhoc' THEN file ELSE null END)) as adhoc_crons_cnt
from tasks_per_slot
group by "start";
```

Display, minute by minute, the number of tasks that were running or still in progress in that minute (successively in a single cron or simultaneously in several or both) :
```sql
create view if not exists tasks_count_per_slot as
select 
	"date",
	"start", 
	sum(CASE WHEN "type" = 'scheduled' THEN 1 ELSE 0 END) as scheduled,
	sum(CASE WHEN "type" = 'adhoc' THEN 1 ELSE 0 END) as adhoc
from tasks_per_slot
group by "start";
```
